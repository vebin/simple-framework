﻿global using Simple.Utils.Models;
global using Simple.Utils.Models.Entity;
global using Simple.Contracts;
global using Simple.AspNetCore;
global using Simple.Utils.Attributes;
global using Simple.Utils;
global using Simple.Utils.Helper;